﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SendServerNotify.aspx.cs" Inherits="PaymentService_AspNet.Yeepay_Other.SendServerNotify" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>补发服务器通知</title>
    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">补发服务器通知</h1>

        <label>
            <span>p1_MerId</span>
            <input id="p1_MerId" type="text" placeholder="商户编号" runat="server" />
        </label>

        <label>
            <span>r0_Cmd</span>
            <input id="r0_Cmd" type="text" placeholder="业务类型" readonly="true" value="" runat="server" />
        </label>

        <label>
            <span>r1_Code</span>
            <input id="r1_Code" type="text" placeholder="支付结果" value="" runat="server" />
        </label>

        <label>
            <span>r2_TrxId</span>
            <input id="r2_TrxId" type="text" placeholder="易宝交易流水号" value="" runat="server" />
        </label>

        <label>
            <span>r3_Amt</span>
            <input id="r3_Amt" type="text" placeholder="支付金额" value="" runat="server" />
        </label>

        <label>
            <span>r4_Cur</span>
            <input id="r4_Cur" type="text" placeholder="交易币种" value="" runat="server" />
        </label>

        <label>
            <span>r5_Pid</span>
            <input id="r5_Pid" type="text" placeholder="商品名称" value="" runat="server" />
        </label>

        <label>
            <span>r6_Order</span>
            <input id="r6_Order" type="text" placeholder="商户订单号" value="" runat="server" />
        </label>

        <label>
            <span>r7_Uid</span>
            <input id="r7_Uid" type="text" placeholder="易宝支付会员ID" value="" runat="server" />
        </label>

        <label>
            <span>r8_MP</span>
            <input id="r8_MP" type="text" placeholder="商户扩展信息" value="" runat="server" />
        </label>

        <label>
            <span>r9_BType</span>
            <select id="r9_BType" runat="server">
                <option value="2">服务器点对点通讯</option>
                <option value="1">浏览器重定向</option>
            </select>
        </label>

        <label>
            <span>rb_BankId</span>
            <input id="rb_BankId" type="text" placeholder="支付通道编码" value="" runat="server" />
        </label>

        <label>
            <span>ro_BankOrderId</span>
            <input id="ro_BankOrderId" type="text" placeholder="银行订单号" value="" runat="server" />
        </label>

        <label>
            <span>rp_PayDate</span>
            <input id="rp_PayDate" type="text" placeholder="支付成功时间" value="" runat="server" />
        </label>

        <label>
            <span>rq_CardNo</span>
            <input id="rq_CardNo" type="text" placeholder="神州充值卡号" value="" runat="server" />
        </label>

        <label>
            <span>ru_Trxtime</span>
            <input id="ru_Trxtime" type="text" placeholder="通知时间" value="" runat="server" />
        </label>

        <label>
            <span>rq_SourceFee</span>
            <input id="rq_SourceFee" type="text" placeholder="用户手续费" value="" runat="server" />
        </label>

        <label>
            <span>rq_TargetFee</span>
            <input id="rq_TargetFee" type="text" placeholder="商户手续费" value="" runat="server" />
        </label>

        <label>
            <span>hmac_safe</span>
            <input id="hmac_safe" type="text" placeholder="安全签名数据" value="" runat="server" />
        </label>

        <label>
            <span>hmac</span>
            <input id="hmac" type="text" placeholder="签名数据" value="" runat="server" />
        </label>

        <label>
            <span>urlNotice</span>
            <input id="urlNotice" type="text" placeholder="通知地址" value="" runat="server" />
        </label>

        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="去发送" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>