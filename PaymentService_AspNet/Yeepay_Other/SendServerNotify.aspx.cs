﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Https;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Sign;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.Yeepay_Other
{
    public partial class SendServerNotify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                p1_MerId.Value = CustomerConfig.merchantAccount;
                r0_Cmd.Value = "Buy";
                r1_Code.Value = "1";
                r4_Cur.Value = "RMB";
                //   r5_Pid.Value = "测试服务器通知";

                urlNotice.Value = "http://localhost:4513/api/v0.1/CallBacks/CallBack/";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = p1_MerId.Value;
            string customerKey = CustomerConfig.merchantKey;

            //***********************修改内容****************************

            string requestUrl = urlNotice.Value;
            //所有请求字段
            string[] list = { "p1_MerId", "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r5_Pid", "r6_Order", "r7_Uid", "r8_MP", "r9_BType", "rb_BankId", "ro_BankOrderId", "rp_PayDate", "rq_CardNo", "ru_Trxtime", "rq_SourceFee", "rq_TargetFee", "hmac_safe", "hmac" };

            //请求需要生成签名的字段
            string[] list_request_tohmac = { "p1_MerId", "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r5_Pid", "r6_Order", "r7_Uid", "r8_MP", "r9_BType" };

            //生成hmac_safe的请求参数
            string[] list_tohmac_safe = { "p1_MerId", "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r5_Pid", "r6_Order", "r7_Uid", "r8_MP", "r9_BType" };

            //请求编码格式
            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            //响应编码格式
            Encoding respEncoding = Encoding.GetEncoding("gb2312");

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            //生成签名字符串
            string data_hmac = ChangeData.toCreateHmacData(map_request, list_request_tohmac);
            log.Append("加密的字符串：" + data_hmac + "\n");

            //生成hmac签名
            string hmac = Digest.CreateHmac(data_hmac, CustomerConfig.merchantKey);
            log.Append("请求hmac：" + hmac + "\n");

            //将hmac添加到请求数据中
            map_request["hmac"] = hmac;

            //生成hmac_safe
            string str_tohmac_safe = ChangeData.toCreateHmacData(map_request, list_tohmac_safe, "#");
            log.Append("加密的字符串：" + str_tohmac_safe + "\n");

            //生成hmac_safe签名
            string hmac_safe = Digest.CreateHmac(str_tohmac_safe, CustomerConfig.merchantKey);
            log.Append("请求hmac：" + hmac_safe + "\n");

            //将hmac添加到请求数据中
            map_request["hmac_safe"] = hmac_safe;

            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list);
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");
            log.Append("************************************分割符*****************************************\n");

            //发送http请求
            string response = SendHttpRequest.payRequest(requestUrl, data_url, true, reqEncoding, respEncoding);
            log.Append("原始返回信息：" + response + "\n");

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            Response.Write(response);
        }
    }
}