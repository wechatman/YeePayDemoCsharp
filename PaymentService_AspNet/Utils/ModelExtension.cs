﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentService_AspNet.Utils
{
    public static class ModelExtension
    {
        public static List<RequestDataField> GetSignFields<T>(this T model) where T : class
        {
            var result = new List<RequestDataField>();
            var props = model.GetType().GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(true);
                if (attrs.Any(attr => attr is IgnoreSignFieldAttribute))
                    continue;
                if (attrs.Any(attr => attr is SignFieldAttribute))
                {
                    foreach (object attr in attrs)
                    {
                        var field = attr as SignFieldAttribute;
                        if (field != null)
                        {
                            var value = new RequestDataField()
                            {
                                Value = prop.GetValue(model, null).GetValueOrDefault(),
                                Sign = true,
                                Order = field.Order
                            };
                            if (string.IsNullOrEmpty(field.FieldName))
                                value.Name = prop.Name;
                            else
                                value.Name = field.FieldName;
                            result.Add(value);
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static SortedDictionary<string, string> GetDataFields<T>(this T model) where T : class
        {
            var result = new SortedDictionary<string, string>();
            var props = model.GetType().GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(true);
                if (attrs.Any(attr => attr is RequestDataFieldAttribute) == false)
                    continue;
                foreach (object attr in attrs)
                {
                    var field = attr as RequestDataFieldAttribute;
                    if (field != null)
                    {
                        var value = prop.GetValue(model, null).GetValueOrDefault();
                        if (string.IsNullOrEmpty(field.FieldName))
                            result.Add(prop.Name, value);
                        else
                            result.Add(field.FieldName, value);
                        break;
                    }
                }
            }
            return result;
        }

        public static string GetPlainText(this IEnumerable<GatewayProperty> list, bool sortByName = true, bool urlEncode = false)
        {
            var param = new List<string>();
            if (sortByName)
                param.AddRange(list.Where(w => w.Signature).OrderBy(o => o.Name).Select(s => string.Concat(s.Name, "=", urlEncode ? HttpUtility.UrlEncode(s.Value) : s.Value)));
            else
                param.AddRange(list.Where(w => w.Signature).Select(s => string.Concat(s.Name, "=", urlEncode ? HttpUtility.UrlEncode(s.Value) : s.Value)));
            return string.Join("&", param);
        }

        public static List<GatewayProperty> ToGatewayPropertyList<T>(this T model, bool skipStringEmpty = false) where T : class
        {
            var result = new List<GatewayProperty>();

            var props = model.GetType().GetProperties();

            foreach (var prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    var gatewayProp = attr as GatewayPropertyAttribute;
                    if (gatewayProp != null)
                    {
                        var value = new GatewayProperty()
                        {
                            Value = prop.GetValue(model, null).GetValueOrDefault(),
                            Signature = gatewayProp.Signature,
                            Order = gatewayProp.Order
                        };

                        if (string.IsNullOrEmpty(gatewayProp.FieldName))
                            value.Name = prop.Name;
                        else
                            value.Name = gatewayProp.FieldName;

                        if (skipStringEmpty && string.IsNullOrEmpty(value.Value))
                            break;

                        result.Add(value);

                        break;
                    }
                }
            }

            return result;
        }

        public static SortedDictionary<string, GatewayProperty> ToGatewayPropertyDictionary<T>(this T model) where T : class
        {
            var result = new SortedDictionary<string, GatewayProperty>();

            var props = model.GetType().GetProperties();

            foreach (var prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    var gatewayProp = attr as GatewayPropertyAttribute;
                    if (gatewayProp != null)
                    {
                        var value = new GatewayProperty()
                        {
                            Value = prop.GetValue(model, null).GetValueOrDefault(),
                            Signature = gatewayProp.Signature,
                            Order = gatewayProp.Order
                        };

                        if (string.IsNullOrEmpty(gatewayProp.FieldName))
                            value.Name = prop.Name;
                        else
                            value.Name = gatewayProp.FieldName;

                        result.Add(value.Name, value);

                        break;
                    }
                }
            }

            return result;
        }
    }
}