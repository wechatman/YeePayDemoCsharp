﻿namespace PaymentService_AspNet.Utils
{
    public class IgnoreSignFieldAttribute : RequestDataFieldAttribute
    {
        public IgnoreSignFieldAttribute()
            : this(string.Empty)
        {
        }

        public IgnoreSignFieldAttribute(string fieldName)
        {
            this.FieldName = fieldName;
            this.Sign = false;
        }
    }
}