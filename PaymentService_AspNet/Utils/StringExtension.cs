﻿namespace PaymentService_AspNet.Utils
{
    public static class StringExtension
    {
        public static string GetValueOrDefault(this object value, bool trim = true)
        {
            if (value == null)
                return string.Empty;

            var strValue = value.ToString();

            if (string.IsNullOrEmpty(strValue))
                return string.Empty;

            if (trim)
                return strValue.Trim();
            else
                return strValue;
        }
    }
}