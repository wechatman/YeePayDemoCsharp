﻿namespace PaymentService_AspNet.Utils
{
    public class RequestDataField
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public bool Sign { get; set; }

        public int Order { get; set; }
    }
}