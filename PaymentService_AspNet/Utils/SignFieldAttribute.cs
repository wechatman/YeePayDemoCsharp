﻿namespace PaymentService_AspNet.Utils
{
    public class SignFieldAttribute : RequestDataFieldAttribute
    {
        public SignFieldAttribute()
            : this(string.Empty)
        {
        }

        public SignFieldAttribute(string fieldName, int order = 0)
        {
            this.FieldName = fieldName;
            this.Sign = true;
            this.Order = order;
        }
    }
}