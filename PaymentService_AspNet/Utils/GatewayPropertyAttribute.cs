﻿using System;

namespace PaymentService_AspNet.Utils
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true)]
    public class GatewayPropertyAttribute : Attribute
    {
        public string FieldName { get; set; }

        public bool Signature { get; set; }

        public int Order { get; set; }

        public GatewayPropertyAttribute(string fieldName, bool signature = false, int order = 0)
        {
            FieldName = fieldName;
            Signature = signature;
            Order = order;
        }
    }

    public class GatewayProperty
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public bool Signature { get; set; }

        public int Order { get; set; }
    }
}