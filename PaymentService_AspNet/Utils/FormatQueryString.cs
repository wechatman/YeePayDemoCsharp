﻿namespace PaymentService_AspNet.Utils
{
    /// <summary>
	/// 将数据进行格式化输出
	/// </summary>
    public abstract class FormatQueryString
    {
        public FormatQueryString()
        {
        }

        /// <summary>
        /// 将类似于：param1=value1&param2=value2的信息串，进行分割，获取。
        /// </summary>
        /// <param name="getParaName">变量名</param>
        /// <param name="requestUrl">数据信息</param>
        /// <param name="valueSplitChar">变量与值得分割符</param>
        /// <param name="strSplitChar">数据间分隔符</param>
        /// <param name="type">原编码格式</param>
        /// <returns></returns>
        public static string GetQueryString(string getParaName, string requestUrl, char valueSplitChar, char strSplitChar, string type)
        {
            string result = "";

            string[] strUrlArg = requestUrl.Split(strSplitChar);

            for (int i = 0; i < strUrlArg.Length; i++)
            {
                if (strUrlArg[i].IndexOf(getParaName + valueSplitChar) >= 0)
                {
                    result = System.Web.HttpUtility.UrlDecode(strUrlArg[i].Split(valueSplitChar)[1], System.Text.Encoding.GetEncoding(type));
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// 网银的信息获取
        /// </summary>
        /// <param name="strParaName"></param>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public static string GetQueryString(string findParaName, string getRequestInfo)
        {
            return GetQueryString(findParaName, getRequestInfo, '=', '&', "gb2312");
        }

        /// <summary>
        /// 网银的信息获取
        /// </summary>
        ///<param name="findParaName">获取的变量名</param>
        /// <returns></returns>
        public static string GetQueryString(string findParaName)
        {
            return GetQueryString(findParaName, System.Web.HttpContext.Current.Request.Url.Query, '=', '&', "gb2312");
        }
    }
}