﻿using System;

namespace PaymentService_AspNet.Utils
{
    public class RequestDataFieldAttribute : Attribute
    {
        public string FieldName { get; set; }

        public bool Sign { get; set; }

        public int Order { get; set; }
    }
}