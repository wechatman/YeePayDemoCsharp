﻿using System.Text;
using PaymentService_AspNet.Config;
using RestSharp;
using RestSharp.Serializers;

namespace PaymentService_AspNet.Https
{
    /// <summary>
    /// 发送http的请求方法
    /// </summary>
    public class SendHttpRequest
    {
        /// <summary>
        /// 发送http请求
        /// </summary>
        /// <param name="requestURL">请求地址</param>
        /// <param name="datastring">请求数据</param>
        /// <param name="post">是否是post</param>
        /// <param name="reqEncoding">请求编码格式</param>
        /// <param name="respEncoding">响应编码格式</param>
        /// <returns></returns>
        public static string payRequest(string requestURL, string datastring, bool post, Encoding reqEncoding, Encoding respEncoding)
        {
            //返回结果
            string responseStr = "";

            if (post)
            {
                responseStr = HttpClass.HttpPost(requestURL, datastring, reqEncoding, respEncoding);
            }
            else
            {
                responseStr = HttpClass.HttpGet(requestURL, datastring, reqEncoding, respEncoding);
            }
            return responseStr;
        }

        public static string PayData(YeePayOrder obj)
        {
            var restClient = new RestClient("https://cha.yeepay.com/app-merchant-proxy/groupTransferController.action")
            {
                Encoding = Encoding.GetEncoding("GBK")
            };
            var restRequest = new RestRequest(Method.POST);

            restRequest.RequestFormat = DataFormat.Xml;
            var ser = new DotNetXmlSerializer();
            ser.Encoding = Encoding.GetEncoding("GBK");
            restRequest.XmlSerializer = ser;

            restRequest.AddXmlBody(obj);

            var restResponse = restClient.Execute(restRequest);
            Encoding encoding = Encoding.GetEncoding("GBK");
            var result = encoding.GetString(restResponse.RawBytes);
            return result;
        }
    }
}