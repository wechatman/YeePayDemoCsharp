﻿using System;

namespace PaymentService_AspNet
{
    public partial class ResultShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            data.Value = Request["data"].Replace("{", "").Replace("}", "").Replace(",", "\n").Replace(":", ":      ").Replace("\"\"", "");
            type.Value = Request["type"];
        }
    }
}