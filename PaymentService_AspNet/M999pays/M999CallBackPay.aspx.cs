﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Sign;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.M999pays
{
    public partial class M999CallBackPay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //**************************************

            //所有的请求信息
            string[] list_all = { "P_UserId", "P_OrderId", "P_CardId", "P_CardPass", "P_FaceValue", "P_ChannelId", "P_PayMoney", "P_ErrCode",
                "P_Subject", "P_Price", "P_Quantity", "P_Description", "P_Notic", "P_ErrMsg", "P_PostKey" };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list_all);
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list_all)
            {
                map_request.Add(param, Request[param]);
            }
            //生成MD5的请求参数
            string[] list_toMD5 = { "P_UserId", "P_OrderId", "P_CardId", "P_CardPass", "P_FaceValue", "P_ChannelId", "P_PayMoney", "P_ErrCode" };

            //编码格式
            Encoding encod = Encoding.GetEncoding("gb2312");

            //**************************************
            string customerNumber = CustomerConfig.merchantAccount;
            string customerKey = CustomerConfig.merchantKey;

            //创建日志对象
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");
            log.Append("通知方式：" + Request.HttpMethod + "\n");

            //存储通知数据
            Dictionary<string, string> info = new Dictionary<string, string>();

            if (Request.HttpMethod.Equals("POST"))
            {
                Stream reqStream = Request.InputStream;
                StreamReader sr = new StreamReader(reqStream, encod);
                string s = sr.ReadToEnd();
                sr.Close();
                reqStream.Close();
                info = URLData.changDictionary(s, list_all, encod);
            }

            if (Request.HttpMethod.Equals("GET"))
            {
                info = URLData.changDictionary(Request.Url.Query, list_all, encod);
            }
            log.Append("通知信息：" + URLData.toStringDictionary(info) + "\n");

            //生成签名字符串
            string data_md5 = ChangeData.ToCreateMD5SourceData(info, list_toMD5, customerKey);
            log.Append("加密的字符串：" + data_md5 + "\n");
            //生成MD5签名
            string P_PostKey = MD5Util.GetMD5(data_md5, "gb2312").ToLower();
            log.Append("请求P_PostKey：" + P_PostKey + "\n");

            //验证签名-------MD5
            string result = Digest.validateParam(P_PostKey, Request["P_PostKey"], "P_PostKey");
            log.Append("验签结果：" + result + "\n");

            //前台信息输出
            data.Value = URLData.toFormatDictionary(info);
            type.Value = result;

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            //服务器通知
            if (result.Contains("成功"))
            {
                Response.Clear();
                Response.Write("errCode=0");
                Response.End();
            }
        }
    }
}