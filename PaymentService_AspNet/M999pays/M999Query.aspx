﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="M999Query.aspx.cs" Inherits="PaymentService_AspNet.M999pays.M999Query" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>merchants.999pays.com网银查询接口请求功能</title>

    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">merchants.999pays.com网银查询接口请求功能</h1>
        <label>
            <span class="important">P_UserId</span>
            <input id="P_UserId" type="text" placeholder="商户编号" runat="server" />
        </label>

        <label>
            <span class="important">P_OrderId</span>
            <input id="P_OrderId" type="text" placeholder=" 商户定单号（要保证唯一），长度最长32 字符 " value="" runat="server" />
        </label>

        <label>
            <span>P_CardId</span>
            <input id="P_CardId" type="text" placeholder="点卡交易时的充值卡卡号 " value="" runat="server" />
        </label>

        <label>
            <span class="important">P_FaceValue</span>
            <input id="P_FaceValue" type="text" placeholder="申明交易金额" value="" runat="server" />
        </label>

        <label>
            <span class="important">P_ChannelId</span>
            <input id="P_ChannelId" type="text" placeholder=" 支付方式，支付方式编码： 1 网银支付 2 Alipay" value="" runat="server" />
        </label>

        <label>
            <span class="important">P_PostKey</span>
            <input id="P_PostKey" type="text" placeholder=" MD5 签名自动产生" value="" runat="server" />
        </label>
        <%--   <label>
            <span class="important">P_Version</span>
            <input id="P_Version" type="text" placeholder="P_Version" value="" runat="server" />
        </label>--%>
        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="葛葛查询" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>