﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="M999DfQuery.aspx.cs" Inherits="PaymentService_AspNet.M999pays.M999DfQuery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>merchants.999pays.com网银代付查询功能</title>

    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">merchants.999pays.com代付查询功能</h1>
        <label>
            <span class="important">version</span>
            <input id="version" type="text" readonly="true" placeholder="接口版本号为 2 " runat="server" />
        </label>

        <label>
            <span class="important">agent_id</span>
            <input id="agent_id" type="text" placeholder=" 商户编号 " value="" runat="server" />
        </label>

        <label>
            <span>batch_no</span>
            <input id="batch_no" type="text" placeholder="批量付款定单号（要保证唯一）。长度最长20 字符 批量付款定单号≤20 " value="" runat="server" />
        </label>

        <label>
            <span class="important">sign</span>
            <input id="sign" type="text" placeholder=" MD5 签名自动产生" value="" runat="server" />
        </label>

        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="代付查询" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>