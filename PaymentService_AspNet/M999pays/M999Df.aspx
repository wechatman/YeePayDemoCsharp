﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="M999Df.aspx.cs" Inherits="PaymentService_AspNet.M999pays.M999Df" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>merchants.999pays.com网银代付功能</title>

    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">merchants.999pays.com网银代付功能</h1>
        <label>
            <span class="important">version</span>
            <input id="version" type="text" readonly="true" placeholder="接口版本号为 2 " runat="server" />
        </label>

        <label>
            <span class="important">agent_id</span>
            <input id="agent_id" type="text" placeholder=" 商户编号 " value="" runat="server" />
        </label>

        <label>
            <span>batch_no</span>
            <input id="batch_no" type="text" placeholder="批量付款定单号（要保证唯一）。长度最长20 字符 批量付款定单号≤20 " value="" runat="server" />
        </label>

        <label>
            <span>batch_amt</span>
            <input id="batch_amt" type="text" placeholder="付款总金额不可为空，单位：元，小数点后保留两位。" value="" runat="server" />
        </label>

        <label>
            <span class="important">batch_num</span>
            <input id="batch_num" type="text" placeholder="该次付款总笔数" value="" runat="server" />
        </label>

        <label>
            <span class="important">detail_data</span>
            <input id="detail_data" type="text" placeholder="商户流水号^银行编号^对公对私^收款人帐号^收款人姓名^付款金额^付款理由^省份^城市^收款支行名称" value="" runat="server" />
        </label>

        <label>
            <span>notify_url</span>
            <input id="notify_url" type="text" placeholder=" 支付后返回的商户处理页面" value="" runat="server" />
        </label>

        <label>
            <span>ext_param1</span>
            <input id="ext_param1" type="text" placeholder="  商户自定义原样返回,长度最长50 字符  " value="" runat="server" />
        </label>

        <label>
            <span class="important">sign</span>
            <input id="sign" type="text" placeholder=" MD5 签名自动产生" value="" runat="server" />
        </label>

        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="去支付" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>