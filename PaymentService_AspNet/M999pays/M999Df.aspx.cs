﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.M999pays
{
    public partial class M999Df : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                version.Value = "2";
                agent_id.Value = CustomerConfig.merchantAccount;
                batch_no.Value = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                batch_num.Value = "1";
                notify_url.Value = "http://" + Request.Url.Authority + "/CallBackPay.aspx";
                ext_param1.Value = "remark";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = CustomerConfig.merchantAccount;
            string customerKey = CustomerConfig.merchantKey;

            //***********************修改内容****************************

            string requestUrl = APIURLConfig.M999DfRequest;

            //所有请求接口数据字段名//get 请求参数顺序
            string[] list = { "agent_id", "batch_amt", "batch_no", "batch_num", "detail_data", "ext_param1", "sign", "notify_url", "version" };
            string[] list_all = { "agent_id", "batch_amt", "batch_no", "batch_num", "detail_data", "ext_param1", "notify_url", "sign", "version" };
            //Dictionary<string, string> testModel = new Dictionary<string, string>();
            //foreach (string param in list)
            //{
            //    testModel.Add(param, Request[param]);
            //}
            //需要生成签名的字段"sign", "notify_url", "version" };签名顺序
            string[] list_tomd5 = { "agent_id", "batch_amt", "batch_no", "batch_num", "detail_data", "ext_param1", "sign", "notify_url", "version" };

            //需要进行转码的字段
            string[] list_changeType = { "" };

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list_all)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            //将Key添加到请求数据中
            map_request["sign"] = "nld3i66z8mzjmzlp5h357ewmgzrf77y8";
            map_request["detail_data"] = batch_no.Value + "^1^0^6222033602001319357^龚文信^200.00^网购单^广东省^广州市^广州名雅支行";

            //4644a1565f38f7fbb67ec3262b4145e6
            // 2018080813344108
            //agent_id=1004521&batch_amt=1&batch_no=2018080813344108&batch_num=1&detail_data=018080720305343^1^0^6222033602001319357^龚文信^200.00^网购单^广东省^广州市^广州名雅支行&ext_param1=remark&key=nld3i66z8mzjmzlp5h357ewmgzrf77y8&notify_url=http://localhost.com&version=2
            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list_tomd5, new string[] { "detail_data", "ext_param1", "notify_url" });
            string hash_data_url = data_url.Replace("sign", "key");
            //将 others data添加到请求数据中
            map_request["sign"] = MD5Util.Md5Hash(hash_data_url.ToLower()).ToLower();

            data_url = URLData.getUrlData(map_request, list, new string[] { "detail_data", "ext_param1" });//传统请求
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");

            //请求编码格式
            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            //响应编码格式
            Encoding respEncoding = Encoding.GetEncoding("gb2312");
            //发送http请求
            //  string response = SendHttpRequest.payRequest(requestUrl, data_url, false, reqEncoding, respEncoding);//传统请求
            SoftLog.LogStr(log.ToString(), theme.InnerText);
            // map_request["notify_url"] = HttpUtility.UrlEncode(map_request["notify_url"]);
            var response = URLData.getQueryData(map_request, list_all);//RestClient封装请求
            Response.Write(response);
        }
    }
}