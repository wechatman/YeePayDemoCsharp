﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="M999Pay.aspx.cs" Inherits="PaymentService_AspNet.M999pays.M999pays" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>merchants.999pays.com网银支付功能</title>

    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">merchants.999pays.com网银支付功能</h1>
        <label>
            <span class="important">P_UserId</span>
            <input id="P_UserId" type="text" readonly="true" placeholder="商户编号" runat="server" />
        </label>

        <label>
            <span class="important">P_OrderId</span>
            <input id="P_OrderId" type="text" placeholder=" 商户定单号（要保证唯一），长度最长32 字符 " value="" runat="server" />
        </label>

        <label>
            <span>P_CardId</span>
            <input id="P_CardId" type="text" placeholder="点卡交易时的充值卡卡号 " value="" runat="server" />
        </label>

        <label>
            <span>P_CardPass</span>
            <input id="P_CardPass" type="text" placeholder="点卡交易时的充充值卡卡密" value="" runat="server" />
        </label>

        <label>
            <span class="important">P_FaceValue</span>
            <input id="P_FaceValue" type="text" placeholder="申明交易金额" value="" runat="server" />
        </label>

        <label>
            <span class="important">P_ChannelId</span>
            <select id="P_ChannelId" runat="server">
                <option value="1">网银</option>
                <option value="36">WAP支付宝</option>
                <option value="121">微信刷卡</option>
                <option value="95">银联扫码(借记卡)</option>
            </select>
        </label>

        <label>
            <span>P_Subject</span>
            <input id="P_Subject" type="text" placeholder="商品标题（最长 20 字符） " value="" runat="server" />
        </label>

        <label>
            <span class="important">P_Price</span>
            <input id="P_Price" type="text" placeholder="商品描述" value="" runat="server" />
        </label>

        <label>
            <span>P_Quantity</span>
            <input id="P_Quantity" type="text" placeholder=" 非必填 商品数量" value="" runat="server" />
        </label>

        <label>
            <span>P_Description</span>
            <select id="P_Description" runat="server">
                <option value="">options</option>
                <option value="10001">中国工商银行</option>
                <option value="10002">中国农业银行</option>
            </select>
        </label>

        <label>
            <span>P_Notic</span>
            <input id="P_Notic" type="text" placeholder=" 非必填 商户备注信息 " value="" runat="server" />
        </label>

        <label>
            <span>P_Result_URL</span>
            <input id="P_Result_URL" type="text" placeholder="callback url" value="" runat="server" />
        </label>

        <label>
            <span>P_Notify_URL</span>
            <input id="P_Notify_URL" type="text" placeholder="非必填 支付后返回的商户显示页面" value="" runat="server" />
        </label>

        <label>
            <span class="important">P_PostKey</span>
            <input id="P_PostKey" type="text" placeholder=" MD5 签名自动产生" value="" runat="server" />
        </label>

        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="去支付" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>