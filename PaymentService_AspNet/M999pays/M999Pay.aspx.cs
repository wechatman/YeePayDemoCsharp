﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.M999pays
{
    public partial class M999pays : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                P_UserId.Value = CustomerConfig.merchantAccount;
                P_OrderId.Value = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                P_FaceValue.Value = "1";
                P_ChannelId.Value = "1";
                P_Price.Value = "1";
                P_Result_URL.Value = "http://" + Request.Url.Authority + "/CallBackPay.aspx";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = CustomerConfig.merchantAccount;
            string customerKey = CustomerConfig.merchantKey;

            //***********************修改内容****************************

            string requestUrl = APIURLConfig.m999pay;

            //所有请求接口数据字段名
            string[] list = { "P_UserId", "P_OrderId", "P_CardId", "P_CardPass", "P_FaceValue", "P_ChannelId", "P_Subject",
                "P_Price", "P_Quantity", "P_Description", "P_Notic", "P_Result_URL", "P_Notify_URL", "P_PostKey" };

            //需要生成签名的字段
            string[] list_tohmac = { "P_UserId", "P_OrderId", "P_CardId", "P_CardPass", "P_FaceValue", "P_ChannelId" };

            //需要进行转码的字段
            string[] list_changeType = { "" };

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            //生成签名字符串
            string data_md5 = ChangeData.ToCreateMD5SourceData(map_request, list_tohmac, customerKey);
            log.Append("加密的字符串：" + data_md5 + "\n");
            //生成MD5签名
            string P_PostKey = MD5Util.GetMD5(data_md5, "gb2312").ToLower();
            log.Append("请求P_PostKey：" + P_PostKey + "\n");

            //将hmac添加到请求数据中
            map_request["P_PostKey"] = P_PostKey;

            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list_tohmac);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            Response.Redirect(requestUrl + "?" + data_url);
        }
    }
}