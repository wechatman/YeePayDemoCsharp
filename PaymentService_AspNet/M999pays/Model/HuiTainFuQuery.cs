﻿using System.Linq;
using Newtonsoft.Json;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.M999pays.Model
{
    public class HuiTainFuQuery
    {
        [GatewayProperty("agent_id", Signature = true, Order = 1)]
        [JsonProperty("agent_id")]
        public string AgentId { get; set; }

        /// <summary>    批量付款定单号（要保证唯一）。长度最长20 字符 批量付款定单号≤20  . </summary>
        /// <value> The batch no. </value>
        [JsonProperty("batch_no")]
        [GatewayProperty("batch_no", Signature = true, Order = 2)]
        public string BatchNo { get; set; }

        /// <summary>  接口版本号为 2. </summary>
        /// <value> The version. </value>
        [JsonProperty("version")]
        [GatewayProperty("version", Signature = true, Order = 4)]
        public string Version { get; set; }

        [JsonProperty("sign")]
        [GatewayProperty("sign", Signature = true, Order = 3)]
        public string Sign { get; set; }

        public string GetSign()
        {
            var param = this.ToGatewayPropertyList()
                .Where(g => g.Signature)
                .OrderBy(g => g.Order)
                .Select(p => $"{p.Name}={p.Value}");

            return string.Join("&", param);
        }
    }
}