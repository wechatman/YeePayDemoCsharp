﻿using System.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.M999pays.Model
{
    [RestSharp.Deserializers.DeserializeAs(Name = "root")]
    [XmlRoot("root")]
    public class HuiTainFuQueryResult
    {
        /// <summary>    返回码值 0000 表示查询成功，其他详见附录 . </summary>
        /// <value> The ret code. </value>
        [RestSharp.Deserializers.DeserializeAs(Name = "ret_code")]
        [XmlElement("ret_code")]
        [GatewayProperty("ret_code", Signature = true, Order = 9)]
        [JsonProperty("ret_code")]
        public string RetCode { get; set; }

        /// <summary>    返回码信息提示 . </summary>
        [RestSharp.Deserializers.DeserializeAs(Name = "ret_msg")]
        [XmlElement("ret_msg")]
        [GatewayProperty("ret_msg", Signature = true, Order = 10)]
        [JsonProperty("ret_msg")]
        public string RetMsg { get; set; }

        [RestSharp.Deserializers.DeserializeAs(Name = "agent_id")]
        [XmlElement("agent_id")]
        [GatewayProperty("agent_id", Signature = true, Order = 1)]
        [JsonProperty("agent_id")]
        public string AgentId { get; set; }

        /// <summary>    付款总金额不可为空，单位：元，小数点后保留两位。. </summary>
        /// <value> The batch amount. </value>
        [RestSharp.Deserializers.DeserializeAs(Name = "batch_amt")]
        [XmlElement("batch_amt")]
        [GatewayProperty("batch_amt", Signature = true, Order = 2)]
        [JsonProperty("batch_amt")]
        public string BatchAmt { get; set; }

        /// <summary>    批量付款定单号（要保证唯一）。长度最长20 字符 批量付款定单号≤20. </summary>
        /// <value> The batch no. </value>
        [RestSharp.Deserializers.DeserializeAs(Name = "batch_no")]
        [XmlElement("batch_no")]
        [JsonProperty("batch_no")]
        [GatewayProperty("batch_no", Signature = true, Order = 3)]
        public string BatchNo { get; set; }

        /// <summary>   该次付款总笔数，付给多少人的数目，“单笔数据集”里面的
        ///        数据总笔数
        ///        . </summary>
        /// <value> The batch number. </value>
        [RestSharp.Deserializers.DeserializeAs(Name = "batch_num")]
        [XmlElement("batch_num")]
        [JsonProperty("batch_num")]
        [GatewayProperty("batch_num", Signature = true, Order = 4)]
        public string BatchNum { get; set; }

        /// <summary>  付款明细,单笔数据集里面按照“商户流水号^收款人帐号^收款人 姓 名^付款金额^付款状态”来组织数据，每条整数据间用“|”符 号分隔
        ///            ，付款状态S 表示付款成功、付款状态 F 表示付款失败、付款状态 P 表示付款处理. </summary>
        [RestSharp.Deserializers.DeserializeAs(Name = "detail_data")]
        [XmlElement("detail_data")]
        [JsonProperty("detail_data")]
        [GatewayProperty("detail_data", Signature = true, Order = 5)]
        public string DetailData { get; set; }

        /// <summary>   商户自定义原样返回,长度最长50 字符 . </summary>
        /// <value> The extent parameter 1. </value>
        [RestSharp.Deserializers.DeserializeAs(Name = "ext_param1")]
        [XmlElement("ext_param1")]
        [JsonProperty("ext_param1")]
        [GatewayProperty("ext_param1", Signature = true, Order = 6)]
        public string ExtParam1 { get; set; }

        [RestSharp.Deserializers.DeserializeAs(Name = "hy_bill_no")]
        [XmlElement("hy_bill_no")]
        [GatewayProperty("hy_bill_no", Signature = true, Order = 7)]
        [JsonProperty("hy_bill_no")]
        public string HyBillNo { get; set; }

        [RestSharp.Deserializers.DeserializeAs(Name = "sign")]
        [XmlElement("sign")]
        [JsonProperty("sign")]
        [GatewayProperty("sign", Signature = true, Order = 8)]
        public string Sign { get; set; }

        public string GetSign()
        {
            var param = this.ToGatewayPropertyList()
                .Where(g => g.Signature)
                .OrderBy(g => g.Order)
                .Select(p => $"{p.Name}={p.Value}");

            return string.Join("&", param);
        }
    }
}