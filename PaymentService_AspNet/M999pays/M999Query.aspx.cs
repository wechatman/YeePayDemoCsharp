﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.M999pays
{
    public partial class M999Query : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                P_UserId.Value = CustomerConfig.merchantAccount;
                //  P_OrderId.Value = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                P_FaceValue.Value = "1";
                P_ChannelId.Value = "1";
                P_CardId.Value = "";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = CustomerConfig.merchantAccount;
            string customerKey = CustomerConfig.merchantKey;

            //***********************修改内容****************************

            string requestUrl = APIURLConfig.m999Query;

            //所有请求接口数据字段名
            string[] list = { "P_UserId", "P_OrderId", "P_ChannelId", "P_CardId", "P_FaceValue", "P_PostKey" };

            //需要生成签名的字段
            string[] list_toMd5 = { "P_UserId", "P_OrderId", "P_ChannelId", "P_CardId", "P_FaceValue", "P_PostKey" };

            //需要进行转码的字段
            string[] list_changeType = { "" };

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            //生成签名元串
            //  string data_md5 = ChangeData.ToCreateMD5SourceData(map_request, list_toMd5, customerKey);
            //log.Append("加密的字符串：" + data_md5 + "\n");
            //生成MD5签名

            //string P_PostKey = MD5Util.GetMD5(data_md5, "gb2312").ToLower();
            //log.Append("请求P_PostKey：" + P_PostKey + "\n");

            //将MD5添加到请求数据中
            map_request["P_PostKey"] = customerKey;

            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list_toMd5);
            string P_PostKey = MD5Util.GetMD5(data_url, "gb2312").ToLower();
            map_request["P_PostKey"] = P_PostKey;

            //  var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");

            //请求编码格式
            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            //响应编码格式
            Encoding respEncoding = Encoding.GetEncoding("gb2312");
            //发送http请求
            // string response = SendHttpRequest.payRequest(requestUrl, data_url, false, reqEncoding, respEncoding);
            SoftLog.LogStr(log.ToString(), theme.InnerText);
            var response = URLData.getQueryData(map_request, list);
            // response
            //P_UserId=1004521&P_OrderId=20180731105440323&P_ChannelId=1&P_CardId=&P_payMoney=1.0000&P_flag=1&P_status=1&P_PostKey=304da26a585a6717fef27776dae282d5

            //
            //验签需更改psotkey为商户密钥
            // string P_ValidatePostKeyResult = MD5Util.GetMD5("P_UserId=1004521&P_OrderId=20180731105440323&P_ChannelId=1&P_CardId=&P_payMoney=1.0000&P_flag=1&P_status=1&P_PostKey=awmliw3pkssb6m2k69e6hintym3riwnk", "gb2312").ToLower();
            Response.Write(response + "<br/>验签:");
        }
    }
}