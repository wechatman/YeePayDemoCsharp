﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Sign;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.Yeepay_PayPage
{
    public partial class OnlinePay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                p0_Cmd.Value = "Buy";
                p1_MerId.Value = CustomerConfig.merchantAccount;
                p2_Order.Value = "Onlince" + Guid.NewGuid().ToString().Replace("-", "o").Substring(0, 9);
                p3_Amt.Value = "0.01";
                p4_Cur.Value = "CNY";
                p5_Pid.Value = "测试order";
                p9_SAF.Value = "0";
                p8_Url.Value = "http://" + Request.Url.Authority + "/CallBackPay.aspx";
                pr_NeedResponse.Value = "1";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = p1_MerId.Value;
            string customerKey = CustomerConfig.merchantKey;

            //***********************修改内容****************************

            string requestUrl = APIURLConfig.pay;

            //所有请求接口数据字段名
            string[] list = { "p0_Cmd", "p1_MerId", "p2_Order", "p3_Amt", "p4_Cur", "p5_Pid", "p6_Pcat", "p7_Pdesc", "p8_Url", "p9_SAF", "pa_MP", "pd_FrpId", "pn_Unit", "pm_Period", "pr_NeedResponse", "pt_UserName", "pt_PostalCode", "pt_Address", "pt_TeleNo", "pt_Mobile", "pt_Email", "pt_LeaveMessage", "hmac", "hmac_safe" };

            //需要生成签名的字段
            string[] list_tohmac = { "p0_Cmd", "p1_MerId", "p2_Order", "p3_Amt", "p4_Cur", "p5_Pid", "p6_Pcat", "p7_Pdesc", "p8_Url", "p9_SAF",
                "pa_MP", "pd_FrpId", "pm_Period", "pn_Unit", "pr_NeedResponse", "pt_UserName", "pt_PostalCode", "pt_Address", "pt_TeleNo", "pt_Mobile",
                "pt_Email", "pt_LeaveMessage" };

            //需要进行转码的字段
            string[] list_changeType = { "p5_Pid" };

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            //生成签名字符串
            string data_hmac = ChangeData.toCreateHmacData(map_request, list_tohmac);
            log.Append("加密的字符串：" + data_hmac + "\n");
            //生成hmac签名
            string hmac = Digest.CreateHmac(data_hmac, CustomerConfig.merchantKey);
            log.Append("请求hmac：" + hmac + "\n");

            //将hmac添加到请求数据中
            map_request["hmac"] = hmac;

            //Add hmac_safe By yongfeng.zhang 20180123
            string data_hmac_safe = ChangeData.toCreateHmacData(map_request, list_tohmac, "#");
            log.Append("安全加密的字符串：" + data_hmac_safe + "\n");
            string hmac_safe = Digest.CreateHmac(data_hmac_safe, CustomerConfig.merchantKey);
            log.Append("请求hmac_safe：" + hmac_safe + "\n");
            map_request["hmac_safe"] = hmac_safe;

            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list_tohmac);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            Response.Redirect(requestUrl + "?" + data_url);
        }
    }
}