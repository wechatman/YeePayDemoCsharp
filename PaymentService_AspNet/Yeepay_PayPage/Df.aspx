﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Df.aspx.cs" Inherits="PaymentService_AspNet.Yeepay_PayPage.Df" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>代付</title>
    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">代付</h1>
        <label>
            <span class="important">cmd </span>
            <input id="cmd" type="text" placeholder="cmd" runat="server" />
        </label>

        <label>
            <span class="important">version</span>
            <input id="version" type="text" placeholder="version" value="" runat="server" />
        </label>

        <label>
            <span class="important">mer_Id</span>
            <input id="mer_Id" type="text" placeholder="mer_Id" value="" runat="server" />
        </label>

        <label>
            <span class="important">group_Id</span>
            <input id="group_Id" type="text" placeholder="group_Id" value="" runat="server" />
        </label>

        <label>
            <span>batch_No</span>
            <input id="batch_No" type="text" placeholder="batch_No" value="" runat="server" />
        </label>

        <label>
            <span class="important">order_Id</span>
            <input id="order_Id" type="text" placeholder="order_Id" value="" runat="server" />
        </label>
        <label>
            <span class="important">bank_Code</span>
            <input id="bank_Code" type="text" placeholder="bank_Code" value="" runat="server" />
        </label>
        <label>
            <span class="important">cnaps</span>
            <input id="cnaps" type="text" placeholder="cnaps" value="" runat="server" />
        </label>
        <label>
            <span class="important">bank_Name</span>
            <input id="bank_Name" type="text" placeholder="bank_Name" value="" runat="server" />
        </label>

        <label>
            <span class="important">branch_Bank_Name</span>
            <input id="branch_Bank_Name" type="text" placeholder="branch_Bank_Name" value="" runat="server" />
        </label>
        <label>
            <span class="important">amount</span>
            <input id="amount" type="text" placeholder="amount" value="" runat="server" />
        </label>
        <label>
            <span class="important">account_Name</span>
            <input id="account_Name" type="text" placeholder="account_Name" value="" runat="server" />
        </label>
        <label>
            <span class="important">account_Number</span>
            <input id="account_Number" type="text" placeholder="account_Number" value="" runat="server" />
        </label>
        <label>
            <span class="important">province</span>
            <input id="province" type="text" placeholder="province" value="" runat="server" />
        </label>
        <label>
            <span class="important">city</span>
            <input id="city" type="text" placeholder="city" value="" runat="server" />
        </label>
        <label>
            <span class="important">fee_Type</span>
            <input id="fee_Type" type="text" placeholder="fee_Type" value="" runat="server" />
        </label>

        <label>
            <span class="important">urgency</span>
            <input id="urgency" type="text" placeholder="urgency" value="" runat="server" />
        </label>
        <label>
            <span class="important">hmac</span>
            <input id="hmac" type="text" placeholder="hmac自动生成" value="" runat="server" />
        </label>

        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="查询" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>