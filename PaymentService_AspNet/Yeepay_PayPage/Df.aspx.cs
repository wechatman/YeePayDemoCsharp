﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;

namespace PaymentService_AspNet.Yeepay_PayPage
{
    public partial class Df : System.Web.UI.Page
    {
        ////***********************DOR DEMO , DO NOT USING IN PRODUCTION ENV (Kevin)****************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cmd.Value = "TransferSingle";
                mer_Id.Value = "";
                batch_No.Value = "";
                order_Id.Value = "";
                bank_Code.Value = "ICBC";
                amount.Value = "1.00";
                account_Name.Value = "";
                account_Number.Value = "";
                fee_Type.Value = "TARGET";
                urgency.Value = "1";
                group_Id.Value = "";
                version.Value = "1.1";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string customerNumber = mer_Id.Value;
            string customerKey = CustomerConfig.merchantKey;
            ////***********************DOR DEMO , DO NOT USING IN PRODUCTION ENV (Kevin)****************************

            string requestUrl = APIURLConfig.DfRequest;
            ////all fields
            string[] list = { "cmd", "version", "mer_Id", "group_Id", "batch_No", "order_Id", "bank_Code", "cnaps", "bank_Name", "branch_Bank_Name", "amount", "account_Name", "account_Number", "province", "city", "fee_Type", "urgency", "hmac", "payee_Mobile", "payee_Email", "leave_Word", "abstractInfo", "remarksInfo" };

            ////all signed fields
            string[] list_request_tohmac = { "cmd", "mer_Id", "batch_No", "order_Id", "amount", "account_Number" };

            //// be  encoding
            string[] list_changeType = { "account_Name" };

            ////response parms
            string[] list_response = { "cmd", "ret_Code", "order_Id", "r1_Code", "bank_Status", "error_Msg", "hmac" };

            ////
            string[] list_response_tohmac = { "cmd", "ret_Code", "r1_Code" };

            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            Encoding respEncoding = Encoding.GetEncoding("gb2312");

            ////***********************************************************

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }

            //data_hmac
            string data_url = "";
            data_url = URLData.getSignData(map_request, list, list_request_tohmac);
            data_url += customerKey;
            string data_hmac = URLData.getSignData(data_url);

            map_request["hmac"] = data_hmac;

            var order = new YeePayOrder();
            order.Cmd = map_request["cmd"];
            order.Version = map_request["version"];
            order.MerId = map_request["mer_Id"];
            order.GroupId = map_request["group_Id"];
            order.BatchNo = map_request["batch_No"];
            order.OrderId = map_request["order_Id"];
            order.AccountName = map_request["account_Name"];
            order.AccountNumber = map_request["account_Number"];
            order.Amount = map_request["amount"];
            order.BankCode = map_request["bank_Code"];
            order.BankName = map_request["bank_Name"];
            order.BranchBankName = map_request["branch_Bank_Name"];
            order.PayeeMobile = map_request["payee_Mobile"];
            order.Cnaps = map_request["cnaps"];
            order.Province = map_request["province"];
            order.City = map_request["city"];
            order.FeeType = map_request["fee_Type"];
            order.Urgency = map_request["urgency"];
            order.PayeeEmail = "";
            order.PayeeMobile = "";
            order.LeaveWord = "";
            order.AbstractInfo = "";
            order.RemarksInfo = "";
            order.Hmac = map_request["hmac"];
            //生成请求参数
            // data_url = URLData.getUrlData(map_request, list, list_request_tohmac);
            //call api
            data_url = URLData.getXmlData(order);
            //string response = SendHttpRequest.payRequest(requestUrl, data_url, true, reqEncoding, respEncoding);
            Response.Write(data_url);
        }
    }
}