﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Yeepay_OnlinePc_C.Yeepay_PayPage
{
    public partial class Recharge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                p0_Cmd.Value = "Load";
                p1_MerId.Value = CustomerConfig.merchantAccount;
                p2_Order.Value = "Onlince" + Guid.NewGuid().ToString().Replace("-", "o").Substring(0, 9);
                p3_Amt.Value = "1.01";
                p4_Cur.Value = "CNY";
                pt_ActId.Value = "0";
                pv_Ver.Value = "2.0";
                p8_Url.Value = "http://" + Request.Url.Authority + "/CallBackPay.aspx";

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = p1_MerId.Value;
            string customerKey = CustomerConfig.merchantKey;


            //***********************修改内容****************************

            string requestUrl = APIURLConfig.recharge;
            //所有请求字段
            string[] list = { "p0_Cmd",
                              "p1_MerId", "p2_Order", "p3_Amt", "p4_Cur", "p5_Pid", "p6_Pcat", "p7_Pdesc", "p8_Url",
                              "pa_MP","pa_Ext", "pb_Oper","pd_FrpId","pd_BankBranch","pt_ActId","pv_Ver", "hmac" , "hmac_safe"};

            //请求需要生成签名的字段
            string[] list_request_tohmac = { "p0_Cmd",
                              "p1_MerId", "p2_Order", "p3_Amt", "p4_Cur", "p5_Pid", "p6_Pcat", "p7_Pdesc", "p8_Url",
                              "pa_MP","pa_Ext", "pb_Oper","pd_FrpId","pd_BankBranch","pt_ActId","pv_Ver" };       

            //请求编码格式
            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            //响应编码格式
            Encoding respEncoding = Encoding.GetEncoding("gb2312");

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");


            //生成签名字符串
            string data_hmac = ChangeData.toCreateHmacData(map_request, list_request_tohmac);
            log.Append("加密的字符串：" + data_hmac + "\n");


            //生成hmac签名
            string hmac = Digest.CreateHmac(data_hmac, CustomerConfig.merchantKey);
            log.Append("请求hmac：" + hmac + "\n");

            //将hmac添加到请求数据中
            map_request["hmac"] = hmac;

            //Add hmac_safe By yongfeng.zhang 20180123
            string data_hmac_safe = ChangeData.toCreateHmacData(map_request, list_request_tohmac, "#");
            log.Append("安全加密的字符串：" + data_hmac_safe + "\n");
            string hmac_safe = Digest.CreateHmac(data_hmac_safe, CustomerConfig.merchantKey);
            log.Append("请求hmac_safe：" + hmac_safe + "\n");
            map_request["hmac_safe"] = hmac_safe;


            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list_request_tohmac);

            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");


            SoftLog.LogStr(log.ToString(), theme.InnerText);

            Response.Redirect(requestUrl + "?" + data_url);




        }
    }
}