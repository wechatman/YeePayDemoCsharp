﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;

namespace PaymentService_AspNet.Yeepay_PayPage
{
    ////***********************DOR DEMO , DO NOT USING IN PRODUCTION ENV (Kevin)****************************
    public partial class DfQuery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cmd.Value = "BatchDetailQuery";
                mer_Id.Value = "";
                batch_No.Value = "";
                order_Id.Value = "";
                query_Mode.Value = "3";
                page_No.Value = "1";
                group_Id.Value = "";
                version.Value = "1.0";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string customerNumber = mer_Id.Value;
            string customerKey = CustomerConfig.merchantKey;

            string requestUrl = APIURLConfig.DfRequest;
            ////all fields
            string[] list = { "cmd", "version", "mer_Id", "group_Id", "batch_No", "query_Mode", "bank_Code", "order_Id", "page_No" };

            ////all signed fields
            string[] list_request_tohmac = { "cmd", "mer_Id", "batch_No", "order_Id", "page_No" };// be  well-ordered

            //// be  encoding
            string[] list_changeType = { "" };

            ////response parms
            string[] list_response = { "" };

            ////
            string[] list_response_tohmac = { "" };

            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            Encoding respEncoding = Encoding.GetEncoding("gb2312");

            ////***********************************************************

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }

            //data_hmac
            string data_url = "";
            data_url = URLData.getSignData(map_request, list, list_request_tohmac);
            data_url += customerKey;
            string data_hmac = URLData.getSignData(data_url);

            map_request["hmac"] = data_hmac;

            var order = new YeePayOrderQuery();
            order.Cmd = map_request["cmd"];
            order.Version = map_request["version"];
            order.MerId = map_request["mer_Id"];
            order.GroupId = map_request["group_Id"];
            order.BatchNo = map_request["batch_No"];
            order.OrderId = map_request["order_Id"];
            order.QueryMode = map_request["query_Mode"];
            order.PageNo = map_request["page_No"];

            order.Hmac = map_request["hmac"];
            //生成请求参数
            // data_url = URLData.getUrlData(map_request, list, list_request_tohmac);
            //call api
            data_url = URLData.getOrderResultXmlData(order);
            //string response = SendHttpRequest.payRequest(requestUrl, data_url, true, reqEncoding, respEncoding);
            Response.Write(data_url);
        }
    }
}