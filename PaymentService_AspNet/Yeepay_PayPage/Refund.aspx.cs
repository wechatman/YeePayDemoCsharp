﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Yeepay_OnlinePc_C.Yeepay_PayPage
{
    public partial class Refund : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                p0_Cmd.Value = "RefundOrd";
                p1_MerId.Value = CustomerConfig.merchantAccount;
                p2_Order.Value = "refund" + Guid.NewGuid().ToString().Replace("-", "r").Substring(0, 10);
                p3_Amt.Value = "0.01";
                p4_Cur.Value = "CNY";

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = p1_MerId.Value;
            string customerKey = CustomerConfig.merchantKey;


            //***********************修改内容****************************

            string requestUrl = APIURLConfig.refund;
            //所有请求字段
            string[] list = { "p0_Cmd", "p1_MerId", "p2_Order", "pb_TrxId", "p3_Amt", "p4_Cur", "p5_Desc", "hmac" ,"hmac_safe"};

            //请求需要生成签名的字段
            string[] list_request_tohmac = { "p0_Cmd", "p1_MerId", "p2_Order", "pb_TrxId", "p3_Amt", "p4_Cur", "p5_Desc" };

            //响应信息
            string[] list_response = { "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r4_Order", "rf_fee", "hmac", "hmac_safe" };

            //响应需要生成签名的字段
            string[] list_response_tohmac = { "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur" };


            string[] list_response_tohmac_safe= { "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur" };

            //请求编码格式
            Encoding reqEncoding = Encoding.GetEncoding("gb2312");
            //响应编码格式
            Encoding respEncoding = Encoding.GetEncoding("gb2312");

            //***********************************************************

            log.Append("接口地址：" + requestUrl + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");

            //存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            //生成签名字符串
            string data_hmac = ChangeData.toCreateHmacData(map_request, list_request_tohmac);
            log.Append("加密的字符串：" + data_hmac + "\n");

            //生成hmac签名
            string hmac = Digest.CreateHmac(data_hmac, CustomerConfig.merchantKey);
            log.Append("请求hmac：" + hmac + "\n");

            //将hmac添加到请求数据中
            map_request["hmac"] = hmac;

            //Add hmac_safe By yongfeng.zhang 20180123
            string data_hmac_safe = ChangeData.toCreateHmacData(map_request, list_request_tohmac, "#");
            log.Append("安全加密的字符串：" + data_hmac_safe + "\n");
            string hmac_safe = Digest.CreateHmac(data_hmac_safe, CustomerConfig.merchantKey);
            log.Append("请求hmac_safe：" + hmac_safe + "\n");
            map_request["hmac_safe"] = hmac_safe;

            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list);
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");
            log.Append("************************************分割符*****************************************\n");

            //发送http请求
            string response = SendHttpRequest.payRequest(requestUrl, data_url, false, reqEncoding, respEncoding);
            log.Append("原始返回信息：" + response + "\n");

            //存储响应信息
            Dictionary<string, string> map_response = new Dictionary<string, string>();
            map_response = URLData.changDictionary(response, list_response, respEncoding, '=', '\n');
            log.Append("响应信息解析：" + URLData.toStringDictionary(map_response) + "\n");

            //重新生成加密字符串
            string str_tohmac = ChangeData.toCreateHmacData(map_response, list_response_tohmac);
            log.Append("加密的字符串：" + str_tohmac + "\n");

            //生成hmac
            string local_hmac = Digest.CreateHmac(str_tohmac, customerKey);
            log.Append("远程请求hmac：" + map_response["hmac"] + "\n");
            log.Append("本地生成hmac：" + local_hmac + "\n");

            //验证签名-------hmac
            string result = Digest.validateParam(local_hmac, map_response["hmac"], "hmac");
            log.Append("响应验签结果：" + result + "\n");

            //重新生成加密字符串
            string str_tohmac_safe = ChangeData.toCreateHmacData(map_response, list_response_tohmac_safe, "#");
            log.Append("加密的字符串：" + str_tohmac_safe + "\n");

            //生成hmac_safe
            string local_hmac_safe = Digest.CreateHmac(str_tohmac_safe, customerKey);
            log.Append("远程请求hmac_safe：" + map_response["hmac_safe"] + "\n");
            log.Append("本地生成hmac_safe：" + local_hmac_safe + "\n");

            //验证签名-------hmac_safe
            string result_safe = Digest.validateParam(local_hmac_safe, map_response["hmac_safe"], "hmac_safe");
            log.Append("响应验签结果：" + result_safe + "\n");

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            string data = Newtonsoft.Json.JsonConvert.SerializeObject(map_response);

            //跳转页面
            Response.Redirect("http://" + Request.Url.Authority + "/Result.aspx?data=" + data + "&type=" + result + result_safe);





        }
    }
}