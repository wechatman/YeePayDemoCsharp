﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Sign;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet
{
    public partial class CallBackPay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //**************************************

            //所有的请求信息
            string[] list_all = { "p1_MerId", "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r5_Pid", "r6_Order", "r7_Uid", "r8_MP", "r9_BType", "rb_BankId", "ro_BankOrderId", "rp_PayDate", "rq_CardNo", "ru_Trxtime", "rq_SourceFee", "rq_TargetFee", "hmac_safe", "hmac" };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(list_all);
            //生成hamc的请求参数
            string[] list_tohmac = { "p1_MerId", "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r5_Pid", "r6_Order", "r7_Uid", "r8_MP", "r9_BType" };

            //生成hmac_safe的请求参数
            string[] list_tohmac_safe = { "p1_MerId", "r0_Cmd", "r1_Code", "r2_TrxId", "r3_Amt", "r4_Cur", "r5_Pid", "r6_Order", "r7_Uid", "r8_MP", "r9_BType" };

            //编码格式
            Encoding encod = Encoding.GetEncoding("gb2312");

            //**************************************
            string customerNumber = CustomerConfig.merchantAccount;
            string customerKey = CustomerConfig.merchantKey;

            //创建日志对象
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");
            log.Append("商户编号：" + customerNumber + "\n");
            log.Append("商户密钥：" + customerKey + "\n");
            log.Append("通知方式：" + Request.HttpMethod + "\n");

            //存储通知数据
            Dictionary<string, string> info = new Dictionary<string, string>();

            if (Request.HttpMethod.Equals("POST"))
            {
                Stream reqStream = Request.InputStream;
                StreamReader sr = new StreamReader(reqStream, encod);
                string s = sr.ReadToEnd();
                sr.Close();
                reqStream.Close();
                info = URLData.changDictionary(s, list_all, encod);
            }

            if (Request.HttpMethod.Equals("GET"))
            {
                info = URLData.changDictionary(Request.Url.Query, list_all, encod);
            }
            log.Append("通知信息：" + URLData.toStringDictionary(info) + "\n");

            //重新生成hmac
            string str_tohmac = ChangeData.toCreateHmacData(info, list_tohmac);
            log.Append("加密的字符串：" + str_tohmac + "\n");

            //生成hmac
            string local_hmac = Digest.CreateHmac(str_tohmac, customerKey);
            log.Append("远程请求hmac：" + info["hmac"] + "\n");
            log.Append("本地生成hmac：" + local_hmac + "\n");

            //验证签名-------hmac
            string result = Digest.validateParam(local_hmac, Request["hmac"], "hmac");
            log.Append("验签结果：" + result + "\n");

            //重新生成hmac
            string str_tohmac_safe = ChangeData.toCreateHmacData(info, list_tohmac_safe, "#");
            log.Append("加密的字符串：" + str_tohmac_safe + "\n");

            //生成hmac_safe
            string local_hmac_safe = Digest.CreateHmac(str_tohmac_safe, customerKey);
            log.Append("远程请求hmac_safe：" + info["hmac"] + "\n");
            log.Append("本地生成hmac_safe：" + local_hmac_safe + "\n");

            //验证签名-------hmac_safe
            string result_safe = Digest.validateParam(local_hmac_safe, Request["hmac_safe"], "hmac_safe");
            log.Append("验签结果：" + result_safe + "\n");

            //前台信息输出
            data.Value = URLData.toFormatDictionary(info);
            type.Value = result + result_safe;

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            //服务器通知
            if (Request["r9_BType"].Equals("2"))
            {
                Response.Clear();
                Response.Write("SUCCESS");
                Response.End();
            }
        }
    }
}