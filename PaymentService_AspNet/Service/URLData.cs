﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using PaymentService_AspNet.Config;
using RestSharp;
using RestSharp.Serializers;
using XmlSerializer = System.Xml.Serialization.XmlSerializer;

namespace PaymentService_AspNet.Service
{
    public class URLData
    {
        public URLData()
        {
        }

        /// <summary>
        /// 根据Key生成value字符串
        /// </summary>
        /// <param name="info">信息数据</param>
        /// <param name="paramsRequest">生成字符串的key值</param>
        /// <param name="paramsDecode">需要专门转码的值</param>
        /// <returns></returns>
        public static string getUrlData(Dictionary<string, string> info, string[] paramsRequest, string[] paramsDecode)
        {
            //返回结果
            string result = "";

            //循环生成信息
            foreach (string param in paramsRequest)
            {
                if (paramsDecode.Contains(param))
                {
                    result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + HttpUtility.UrlEncode(info[param], Encoding.GetEncoding("gb2312")));
                }
                else
                {
                    result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + info[param]);
                }
            }

            return result;
        }

        public static string getUrlData(Dictionary<string, string> info, string[] paramsRequest, string[] paramsDecode, string[] noEncode)
        {
            //返回结果
            string result = "";

            //循环生成信息
            foreach (string param in paramsRequest)
            {
                if (paramsDecode.Contains(param))
                {
                    if (!noEncode.Contains(param))
                    {
                        result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + HttpUtility.UrlEncode(info[param], Encoding.GetEncoding("gb2312")));
                    }
                    else
                    {
                        result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + info[param]);
                    }
                }
                else
                {
                    result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + info[param]);
                }
            }

            return result;
        }

        public static string getQueryData(Dictionary<string, string> info, string[] list)
        {
            var encoding = Encoding.GetEncoding("GBK");
            var restClient = new RestClient("https://gateway.999pays.com/Payment/BatchTransfer.aspx")//https://gateway.999pays.com/Pay/Query.aspx
            {
                Encoding = encoding
            };
            var restRequest = new RestRequest(Method.GET);

            foreach (string param in list)
            {
                restRequest.AddParameter(param, info[param]);
            }

            var restResponse = restClient.Execute(restRequest);

            var result = Encoding.GetEncoding("GBK").GetString(restResponse.RawBytes);
            return result;
        }

        /// <summary>   Gets XML data. </summary>
        /// <returns>   The XML data. </returns>
        public static string getXmlData(YeePayOrder obj)
        {
            var restClient = new RestClient("https://cha.yeepay.com/app-merchant-proxy/groupTransferController.action")
            {
                Encoding = Encoding.GetEncoding("GBK")
            };
            var restRequest = new RestRequest(Method.POST);

            restRequest.RequestFormat = DataFormat.Xml;
            var ser = new DotNetXmlSerializer();
            ser.Encoding = Encoding.GetEncoding("GBK");
            restRequest.XmlSerializer = ser;

            restRequest.AddXmlBody(obj);

            // var restResponse = restClient.Execute(restRequest);
            // Encoding encoding = Encoding.GetEncoding("GBK");
            // var result = encoding.GetString(restResponse.RawBytes);
            // return result;
            return null;
        }

        public static string getOrderResultXmlData(YeePayOrderQuery obj)
        {
            var restClient = new RestClient("https://cha.yeepay.com/app-merchant-proxy/groupTransferController.action")
            {
                Encoding = Encoding.GetEncoding("GBK")
            };
            var restRequest = new RestRequest(Method.POST);

            restRequest.RequestFormat = DataFormat.Xml;
            var ser = new DotNetXmlSerializer();
            ser.Encoding = Encoding.GetEncoding("GBK");
            restRequest.XmlSerializer = ser;

            restRequest.AddXmlBody(obj);

            var restResponse = restClient.Execute<YeePayQueryResult>(restRequest);
            Encoding encoding = Encoding.GetEncoding("GBK");
            var result = encoding.GetString(restResponse.RawBytes);

            //RestSharp.Deserializers.XmlDeserializer deserial = new XmlDeserializer();
            //  var respModel = deserial.Deserialize<YeePayQueryResult>(restResponse);
            //  //https://www.cnblogs.com/Jerseyblog/p/7404283.html
            //  https://github.com/restsharp/RestSharp/issues/758
            using (MemoryStream memoryStream = new MemoryStream(encoding.GetBytes(result)))
            {
                var respModel = new XmlSerializer(typeof(YeePayQueryResult)).Deserialize((Stream)memoryStream);
                return Newtonsoft.Json.JsonConvert.SerializeObject(respModel);
            }
        }

        public static string getSignData(string src)
        {
            var restClient = new RestClient("http://192.168.0.26:16800/sign");
            var restRequest = new RestRequest(Method.GET);
            restRequest.AddQueryParameter("req", src);

            var restResponse = restClient.Execute(restRequest);
            return restResponse.Content;
        }

        public static string getSignData(Dictionary<string, string> info, string[] paramsRequest, string[] paramsDecode)
        {
            //返回结果
            string result = "";

            //循环生成信息
            foreach (string param in paramsDecode)
            {
                if (!string.IsNullOrEmpty(info[param]))
                {
                    result += (info[param]);
                }
            }

            return result;
        }

        /// <summary>
        /// 将NameValueCollection  转成   Dictionary<string,string>
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static Dictionary<string, string> changDictionary(NameValueCollection info)
        {
            //返回结果
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (string key in info.Keys)
            {
                result.Add(key, info[key]);
            }

            return result;
        }

        /// <summary>
        /// 将string类型的字符串转化成Dictionary(网银)
        /// </summary>
        /// <param name="str"></param>
        /// <param name="paramNames"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Dictionary<string, string> changDictionary(string str, string[] paramNames, Encoding type)
        {
            //返回结果
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string param in paramNames)
            {
                result.Add(param, GetQueryString(param, str, '=', '&', type));
            }

            return result;
        }

        /// <summary>
        /// 将string类型的字符串转化成Dictionary
        /// </summary>
        /// <param name="str">含有数据信息的字符串</param>
        /// <param name="paramNames">需要的key的数组</param>
        /// <param name="type">编码格式</param>
        /// <param name="strSplitChar">键值对之间的分隔符</param>
        /// <param name="valueSplitChar">键值之间的分割符</param>
        /// <returns></returns>
        public static Dictionary<string, string> changDictionary(string str, string[] paramNames, Encoding type, char valueSplitChar, char strSplitChar)
        {
            //返回结果
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string param in paramNames)
            {
                result.Add(param, GetQueryString(param, str, valueSplitChar, strSplitChar, type));
            }

            return result;
        }

        /// <summary>
        /// 将Dictionary<string, string>转化成string
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string toStringDictionary(Dictionary<string, string> info)
        {
            //返回结果
            StringBuilder sb = new StringBuilder();

            //循环生成
            foreach (string key in info.Keys)
            {
                sb.Append("[" + key + "," + info[key] + "]  ");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 转化成前台信息输出
        /// </summary>
        /// <param name="info">输出的信息</param>
        /// <returns></returns>
        public static string toFormatDictionary(Dictionary<string, string> info)
        {
            //返回结果
            StringBuilder sb = new StringBuilder();

            //循环生成
            foreach (string key in info.Keys)
            {
                sb.Append("  [" + key + "]" + "：" + info[key] + "\n");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 将类似于：param1=value1&param2=value2的信息串，进行分割，获取。
        /// </summary>
        /// <param name="getParaName">变量名</param>
        /// <param name="requestUrl">数据信息</param>
        /// <param name="valueSplitChar">变量与值得分割符</param>
        /// <param name="strSplitChar">数据间分隔符</param>
        /// <param name="type">原编码格式</param>
        /// <returns></returns>
        public static string GetQueryString(string getParaName, string requestUrl, char valueSplitChar, char strSplitChar, Encoding type)
        {
            string result = "";

            string[] strUrlArg = requestUrl.Split(strSplitChar);

            for (int i = 0; i < strUrlArg.Length; i++)
            {
                if (strUrlArg[i].IndexOf(getParaName + valueSplitChar) >= 0)
                {
                    result = System.Web.HttpUtility.UrlDecode(strUrlArg[i].Split(valueSplitChar)[1], type);
                    break;
                }
            }
            return result;
        }
    }
}