﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CallBackPay.aspx.cs" Inherits="PaymentService_AspNet.CallBackPay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>网银支付通知</title>
    <link href="css/temp.css" rel="Stylesheet" type="text/css" />
</head>

<body>
    <form method="post" class="smart-green" runat="server">

        <h1 id="theme" runat="server">网银通知信息</h1>

        <label>
            <span>易宝支付回调返回的信息为：</span>
            <textarea id="data" rows="15" cols="30" runat="server"></textarea>
        </label>

        <label>
            <span>返回的信息类别</span>
            <input id="type" type="text" placeholder="" runat="server" />
        </label>
    </form>
</body>
</html>