﻿namespace PaymentService_AspNet.Config
{
    public class APIURLConfig
    {
        static APIURLConfig()
        {
            //网银支付地址
            pay = "https://www.yeepay.com/app-merchant-proxy/node";
            m999pay = "https://gateway.999pays.com/Pay/KDBank.aspx";
            dob169Pay = "https://gwbb69.169.cc/interface/AutoBank/index.aspx";
            //查询地址
            QueryableOrder = "https://cha.yeepay.com/app-merchant-proxy/command";
            m999Query = "https://gateway.999pays.com/Pay/Query.aspx";
            m999DfQuery = "https://gateway.999pays.com/Payment/BatchQuery.aspx";
            //单笔退款接口
            refund = "https://cha.yeepay.com/app-merchant-proxy/command";
            //退款查询
            refundOrder = "https://www.yeepay.com/app-merchant-proxy/node";
            //单笔订单撤销
            orderBack = "https://cha.yeepay.com/app-merchant-proxy/command";
            //充值支付
            recharge = "http://www.yeepay.com/app-merchant-proxy/node";

            DfRequest = "https://cha.yeepay.com/app-merchant-proxy/groupTransferController.action";
            M999DfRequest = "https://gateway.999pays.com/Payment/BatchTransfer.aspx";
        }

        /// <summary>
        /// 单笔订单撤销
        /// </summary>
        public static string orderBack { get; private set; }

        /// <summary>
        /// 网银支付地址
        /// </summary>
        public static string pay { get; private set; }

        /// <summary>
        ///m999pay 网银支付地址
        /// </summary>
        public static string m999pay { get; private set; }

        public static string m999DfQuery { get; private set; }
        public static string dob169Pay { get; private set; }

        /// <summary>
        /// 查询地址
        /// </summary>
        public static string QueryableOrder { get; private set; }

        /// <summary>
        /// 查询地址
        /// </summary>
        public static string m999Query { get; private set; }

        /// <summary>
        /// 充值支付
        /// </summary>
        public static string recharge { get; private set; }

        /// <summary>
        /// 单笔退款接口
        /// </summary>
        public static string refund { get; private set; }

        /// <summary>
        /// 退款查询
        /// </summary>
        public static string refundOrder { get; private set; }

        /// <summary>
        /// DF
        /// </summary>
        public static string DfRequest { get; private set; }

        public static string M999DfRequest { get; private set; }
    }
}