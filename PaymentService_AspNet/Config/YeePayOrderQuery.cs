﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace PaymentService_AspNet.Config
{
    [XmlRoot("data")]
    public class YeePayOrderQuery
    {
        [XmlElement("cmd")]
        [JsonProperty("cmd")]
        public string Cmd { get; set; }

        [XmlElement("version")]
        [JsonProperty("version")]
        public string Version { get; set; }

        [XmlElement("mer_Id")]
        [JsonProperty("mer_Id")]
        public string MerId { get; set; }

        /// <summary>
        /// 总公司商户 编号
        /// </summary>
        /// <value>The group identifier.</value>
        [XmlElement("group_Id")]
        [JsonProperty("group_Id")]
        public string GroupId { get; set; }

        [XmlElement("batch_No")]
        [JsonProperty("batch_No")]
        public string BatchNo { get; set; }

        [XmlElement("order_Id")]
        [JsonProperty("order_Id")]
        public string OrderId { get; set; }

        [XmlElement("page_No")]
        [JsonProperty("page_No")]
        public string PageNo { get; set; }

        [XmlElement("query_Mode")]
        [JsonProperty("query_Mode")]
        public string QueryMode { get; set; }

        [XmlElement("hmac")]
        [JsonProperty("hmac")]
        public string Hmac { get; set; }
    }
}