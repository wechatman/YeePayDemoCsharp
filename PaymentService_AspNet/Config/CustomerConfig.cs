﻿namespace PaymentService_AspNet.Config
{
    public class CustomerConfig
    {
        static CustomerConfig()
        {
            //网关
            merchantAccount = "";
            merchantKey = "";
        }

        /// <summary>
        /// 商户编号
        /// </summary>
        public static string merchantAccount { get; set; }

        /// <summary>
        /// 商户密钥
        /// </summary>
        public static string merchantKey { get; private set; }
    }
}