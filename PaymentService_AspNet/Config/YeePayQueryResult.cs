﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace PaymentService_AspNet.Config
{
    [XmlRoot(ElementName = "item")]
    public class ContentItem
    {
        [XmlElement(ElementName = "order_Id")]
        public string order_Id { get; set; }

        [XmlElement(ElementName = "payee_Bank_Account")]
        public string payee_Bank_Account { get; set; }

        [XmlElement(ElementName = "remarksInfo")]
        public string remarksInfo { get; set; }

        [XmlElement(ElementName = "refund_Date")]
        public string refund_Date { get; set; }

        [XmlElement(ElementName = "real_pay_amount")]
        public string real_pay_amount { get; set; }

        [XmlElement(ElementName = "payee_BankName")]
        public string payee_BankName { get; set; }

        [XmlElement(ElementName = "complete_Date")]
        public string complete_Date { get; set; }

        [XmlElement(ElementName = "request_Date")]
        public string request_Date { get; set; }

        [XmlElement(ElementName = "amount")]
        public string amount { get; set; }

        [XmlElement(ElementName = "fee")]
        public string fee { get; set; }

        [XmlElement(ElementName = "payee_Name")]
        public string payee_Name { get; set; }

        [XmlElement(ElementName = "abstractInfo")]
        public string abstractInfo { get; set; }

        [XmlElement(ElementName = "bank_Status")]
        public string bank_Status { get; set; }

        [XmlElement(ElementName = "r1_Code")]
        public string r1_Code { get; set; }

        [XmlElement(ElementName = "fail_Desc")]
        public string fail_Desc { get; set; }

        [XmlElement(ElementName = "note")]
        public string note { get; set; }
    }

    [XmlRoot(ElementName = "items")]
    public class ContentItems
    {
        public List<ContentItem> items { get; set; }
    }

    [XmlRoot(ElementName = "data")]
    public class YeePayQueryResult
    {
        [XmlElement(ElementName = "cmd")]
        public string cmd { get; set; }

        [XmlElement(ElementName = "ret_Code")]
        public string ret_Code { get; set; }

        [XmlElement(ElementName = "error_Msg")]
        public string error_Msg { get; set; }

        [XmlElement(ElementName = "batch_No")]
        public string batch_No { get; set; }

        [XmlElement(ElementName = "total_Num")]
        public string total_Num { get; set; }

        [XmlElement(ElementName = "end_Flag")]
        public string end_Flag { get; set; }

        [XmlElement(ElementName = "list")]
        public List<ContentItems> list { get; set; }

        [XmlElement(ElementName = "hmac")]
        public string hmac { get; set; }
    }
}