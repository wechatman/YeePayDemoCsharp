﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace PaymentService_AspNet.Config
{
    [XmlRoot("data")]
    public class YeePayOrder
    {
        [XmlElement("cmd")]
        [JsonProperty("cmd")]
        public string Cmd { get; set; }

        [XmlElement("version")]
        [JsonProperty("version")]
        public string Version { get; set; }

        [XmlElement("mer_Id")]
        [JsonProperty("mer_Id")]
        public string MerId { get; set; }

        /// <summary>
        /// 总公司商户 编号
        /// </summary>
        /// <value>The group identifier.</value>
        [XmlElement("group_Id")]
        [JsonProperty("group_Id")]
        public string GroupId { get; set; }

        [XmlElement("batch_No")]
        [JsonProperty("batch_No")]
        public string BatchNo { get; set; }

        [XmlElement("order_Id")]
        [JsonProperty("order_Id")]
        public string OrderId { get; set; }

        [XmlElement("bank_Code")]
        [JsonProperty("bank_Code")]
        public string BankCode { get; set; }

        [XmlElement("cnaps")]
        [JsonProperty("cnaps")]
        public string Cnaps { get; set; }

        [XmlElement("bank_Name")]
        [JsonProperty("bank_Name")]
        public string BankName { get; set; }

        [XmlElement("branch_Bank_Name")]
        [JsonProperty("branch_Bank_Name")]
        public string BranchBankName { get; set; }

        [XmlElement("amount")]
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [XmlElement("account_Name")]
        [JsonProperty("account_Name")]
        public string AccountName { get; set; }

        [XmlElement("account_Number")]
        [JsonProperty("account_Number")]
        public string AccountNumber { get; set; }

        [XmlElement("province")]
        [JsonProperty("province")]
        public string Province { get; set; }

        [XmlElement("city")]
        [JsonProperty("city")]
        public string City { get; set; }

        [XmlElement("fee_Type")]
        [JsonProperty("fee_Type")]
        public string FeeType { get; set; }

        [XmlElement("payee_Email")]
        public string PayeeEmail { get; set; }

        [XmlElement("payee_Mobile")]
        public string PayeeMobile { get; set; }

        [XmlElement("leave_Word")]
        public string LeaveWord { get; set; }

        [XmlElement("abstractInfo")]
        public string AbstractInfo { get; set; }

        [XmlElement("remarksInfo")]
        public string RemarksInfo { get; set; }

        [XmlElement("urgency")]
        [JsonProperty("urgency")]
        public string Urgency { get; set; }

        [XmlElement("hmac")]
        [JsonProperty("hmac")]
        public string Hmac { get; set; }
    }
}