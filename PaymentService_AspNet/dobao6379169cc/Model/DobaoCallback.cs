﻿using System.Linq;
using Newtonsoft.Json;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.dobao6379169cc.Model
{
    public class DobaoCallback
    {
        [JsonProperty("orderid")]
        [GatewayProperty("orderid", Signature = true, Order = 1)]
        public string Orderid { get; set; }

        [JsonProperty("opstate")]
        [GatewayProperty("opstate", Signature = true, Order = 2)]
        public string Opstate { get; set; }

        [JsonProperty("ovalue")]
        [GatewayProperty("ovalue", Signature = false, Order = 3)]
        public string Ovalue { get; set; }

        [JsonProperty("systime")]
        public string Systime { get; set; }

        [JsonProperty("sysorderid")]
        public string Sysorderid { get; set; }

        [JsonProperty("completiontime")]
        public string Completiontime { get; set; }

        [JsonProperty("attach")]
        public string Attach { get; set; }

        [JsonProperty("msg")]
        public string Msg { get; set; }

        public string GetSign(string mKey)
        {
            var param = this.ToGatewayPropertyList()
                .Where(g => g.Signature)
                .OrderBy(g => g.Order)
                .Select(p => $"{p.Name}={p.Value}");
            var hashData = string.Join("&", param);
            return hashData + mKey;
        }
    }
}