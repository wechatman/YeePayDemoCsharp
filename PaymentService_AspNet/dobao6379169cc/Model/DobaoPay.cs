﻿using System.Linq;
using Newtonsoft.Json;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.dobao6379169cc.Model
{
    public class DobaoPay
    {
        [JsonProperty("parter")]
        [GatewayProperty("parter", Signature = true, Order = 1)]
        public string Parter { get; set; }

        [JsonProperty("type")]
        [GatewayProperty("type", Signature = true, Order = 2)]
        public string Type { get; set; }

        [JsonProperty("value")]
        [GatewayProperty("value", Signature = true, Order = 3)]
        public string Value { get; set; }

        [JsonProperty("orderid")]
        [GatewayProperty("orderid", Signature = true, Order = 4)]
        public string Orderid { get; set; }

        [JsonProperty("callbackurl")]
        [GatewayProperty("callbackurl", Signature = true, Order = 5)]
        public string Callbackurl { get; set; }

        [JsonProperty("hrefbackurl")]
        [GatewayProperty("hrefbackurl", Signature = false, Order = 6)]
        public string Hrefbackurl { get; set; }

        [JsonProperty("attach")]
        [GatewayProperty("attach", Signature = false, Order = 7)]
        public string Attach { get; set; }

        [JsonProperty("sign")]
        [GatewayProperty("sign", Signature = false, Order = 8)]
        public string Sign { get; set; }

        public string GetSign(string mKey)
        {
            var param = this.ToGatewayPropertyList()
                .Where(g => g.Signature)
                .OrderBy(g => g.Order)
                .Select(p => $"{p.Name}={p.Value}");
            var hashData = string.Join("&", param);
            return hashData + mKey;
        }

        public string GetUrlSegment()
        {
            var param = this.ToGatewayPropertyDictionary()
                .Where(g => !string.IsNullOrEmpty(g.Value.Value))
                .OrderBy(g => g.Value.Order)
                .Select(p => $"{p.Key}={p.Value.Value}");
            return string.Join("&", param);
        }
    }
}