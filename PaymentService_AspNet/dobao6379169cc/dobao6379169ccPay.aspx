﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dobao6379169ccPay.aspx.cs" Inherits="PaymentService_AspNet.dobao6379169cc.dobao6379169ccPay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>dobao多宝 6379.169.cc Pay网银支付功能</title>

    <link href="../css/temp.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form method="post" class="smart-green" runat="server">
        <h1 id="theme" runat="server">dobao多宝 6379.169.cc Pay网银支付功能</h1>
        <label>
            <span class="important">parter</span>
            <input id="parter" type="text" readonly="true" placeholder="商户编号" runat="server" />
        </label>

        <label>
            <span class="important">type</span>
            <select id="type" runat="server">
                <option value="1004">PC微信扫码 </option>
                <option value="1007">适配手机界面的微信扫码</option>
                <option value="1006">适配手机界面的支付宝扫码</option>
                <option value="1001">银联扫码</option>
            </select>
        </label>

        <label>
            <span>value </span>
            <input id="value" type="text" placeholder="单位元（人民币） ，2位小数，最小支付金额为1.00,微信支付宝至少2元  " value="" runat="server" />
        </label>

        <label>
            <span>orderid </span>
            <input id="orderid" type="text" placeholder="商户系统订单号，该订单号将作为多宝接口的返回数据。该值需在商户系统内唯一 " value="" runat="server" />
        </label>

        <label>
            <span class="important">callbackurl</span>
            <input id="callbackurl" type="text" placeholder="异步通知过程的返回地址，需要以http://开头且没有任何参" value="" runat="server" />
        </label>
        <label>
            <span class="important">hrefbackurl</span>
            <input id="hrefbackurl" type="text" placeholder="同步通知过程的返回地址(在支付完成后多宝接" value="" runat="server" />
        </label>
        <label>
            <span class="important">attach</span>
            <input id="attach" type="text" placeholder="备注信息，中会原样返回。若该值包含中文，请注意编码  " value="" runat="server" />
        </label>

        <label>
            <span class="important">sign</span>
            <input id="sign" type="text" placeholder=" MD5 签名自动产生，GB2312编码 " value="" runat="server" />
        </label>

        <label>
            <span>&nbsp;</span>
            <asp:Button ID="Button2" runat="server" class="button" Text="去支付" OnClick="Button1_Click" />
        </label>
    </form>
</body>
</html>