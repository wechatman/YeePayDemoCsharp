﻿using System;
using System.Collections.Generic;
using System.Text;
using PaymentService_AspNet.Config;
using PaymentService_AspNet.dobao6379169cc.Model;
using PaymentService_AspNet.Service;
using PaymentService_AspNet.Utils;

namespace PaymentService_AspNet.dobao6379169cc
{
    public partial class dobao6379169ccPay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                parter.Value = CustomerConfig.merchantAccount;
                type.Value = "1004";
                value.Value = "2";
                orderid.Value = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                hrefbackurl.Value = "";
                callbackurl.Value = "http://" + Request.Url.Authority + "/CallBackPay.aspx";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder log = new StringBuilder();
            log.Append(DateTime.Now.ToString() + "\n");
            log.Append("测试功能：" + theme.InnerText + "\n");

            string customerNumber = CustomerConfig.merchantAccount;
            string customerKey = CustomerConfig.merchantKey;
            var encoding = Encoding.GetEncoding("gb2312");
            var model = new DobaoPay
            {
                Parter = parter.Value,
                Type = type.Value,
                Value = value.Value,
                Orderid = orderid.Value,
                Callbackurl = callbackurl.Value
            };
            var hashData = model.GetSign(customerKey);
            var sign = MD5Util.Md5Hash(hashData).ToLower();
            model.Sign = sign;

            string requestUrl = APIURLConfig.dob169Pay;
            //var restClient =
            //    new RestClient(requestUrl) { Encoding = encoding };
            //var restRequest = new RestRequest(Method.POST);
            //foreach (var param in model.ToGatewayPropertyDictionary())
            //{
            //    restRequest.AddParameter(param.Key, param.Value.Value);
            //}
            //var restResponse = restClient.Execute<HuiTainFuQueryResult>(restRequest);
            //DotNetXmlDeserializer deserial = new DotNetXmlDeserializer();
            //var respModel = deserial.Deserialize<HuiTainFuQueryResult>(restResponse);

            //var responseContent = Encoding.GetEncoding("GBK").GetString(restResponse.RawBytes);
            //***********************修改内容****************************

            //string requestUrl = APIURLConfig.m999pay;

            ////所有请求接口数据字段名
            string[] list = { "parter", "type", "value", "orderid", "callbackurl", "hrefbackurl", "attach", "sign" };
            //Dictionary<string, string> testModel = new Dictionary<string, string>();
            //foreach (string param in list)
            //{
            //    testModel.Add(param, Request[param]);
            //}
            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(testModel);
            ////需要生成签名的字段
            string[] list_toMd5 = { "P_UserId", "P_OrderId", "P_CardId", "P_CardPass", "P_FaceValue", "P_ChannelId" };

            //需要进行转码的字段
            // string[] list_changeType = { "" };

            //***********************************************************

            //log.Append("接口地址：" + requestUrl + "\n");
            //log.Append("商户编号：" + customerNumber + "\n");
            //log.Append("商户密钥：" + customerKey + "\n");

            ////存储前台数据
            Dictionary<string, string> map_request = new Dictionary<string, string>();
            foreach (string param in list)
            {
                map_request.Add(param, Request[param]);
            }
            log.Append("请求信息：" + URLData.toStringDictionary(map_request) + "\n");

            ////生成签名字符串
            //string data_md5 = ChangeData.ToCreateMD5SourceData(map_request, list_tohmac, customerKey);
            //log.Append("加密的字符串：" + data_md5 + "\n");
            ////生成MD5签名
            //string P_PostKey = MD5Util.GetMD5(data_md5, "gb2312").ToLower();
            //log.Append("请求P_PostKey：" + P_PostKey + "\n");

            ////将MD5添加到请求数据中
            map_request["sign"] = model.Sign;

            //生成请求数据
            string data_url = "";
            data_url = URLData.getUrlData(map_request, list, list_toMd5);
            data_url = model.GetUrlSegment();
            log.Append("请求链接：" + requestUrl + "?" + data_url + "\n");

            SoftLog.LogStr(log.ToString(), theme.InnerText);

            Response.Redirect(requestUrl + "?" + data_url);
        }
    }
}